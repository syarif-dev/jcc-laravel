<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Form Sign Up</title>
    </head>
    <body>
        <h3>Buat Account Baru</h3>

        <h5>Sign Up Form</h5>

        <form action="{{ route('welcome') }}" method="POST">
            @csrf
            <label for="firstName">First name</label>
            <br /><br />
            <input type="text" name="firstName" />
            <br /><br />

            <label for="lastName">Last name</label>
            <br /><br />
            <input type="text" name="lastName" />
            <br /><br />

            <label for="gender">Gender</label>
            <br /><br />
            <input type="radio" name="gender" id="Male" value="Male" />Male
            <br />
            <input
                type="radio"
                name="gender"
                id="Female"
                value="Female"
            />Female <br /><br />

            <label for="nationality">Nationality</label>
            <br /><br />
            <select name="nationality" id="nationality">
                <option value="Indonesia">Indonesia</option>
                <option value="Singapura">Singapura</option>
                <option value="Amerika">Amerika</option>
                <option value="Inggris">Inggris</option>
            </select>
            <br /><br />

            <label for="language">Language Spoken</label>
            <br /><br />
            <input type="checkbox" name="language" id="indonesia" />Bahasa
            Indonesia<br />
            <input type="checkbox" name="language" id="english" />English<br />
            <input type="checkbox" name="language" id="arabic" />Arabic<br />
            <input type="checkbox" name="language" id="japanese" />Japanese<br />
            <br /><br />

            <label for="bio">Bio</label>
            <br /><br />
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
            <br />

            <input type="submit" value="Sign Up" />
        </form>
    </body>
</html>
